# Q Time é Teu? #

O aplicativo Que time é teu? é um jogo, que testa a capacidade do usuário de identificar corretamente de que estado é o escudo sugerido na lista de dez perguntas. Existem três modos de dificuldade: fácil, médio e difícil, cada um com suas peculiaridades e ainda um prazo de 30 segundos para dar a resposta correta.
A maioria dos escudos foram fornecidos pelas empresas que produzem o material esportivo dos times, das assessorias dos clubes e outros baixados do site www.brandsoftheworld.com. Foram utilizados arquivos vetoriais para dar mais qualidade na visualização.

A 26ª Copa do Brasil 2014 é tema deste Quiz. Que time é teu? é uma mistura de arte, futebol e geografia. A proposta é distrair você nos intervalos dos jogos ou nas horas vagas e gerar conhecimento desta arte que contagiou o mundo.

Divirtam-se!

### Arquitetura ###

Ainda é preciso melhorar arquitetura do aplicativo.