package com.allan.quetimeeteu;

import android.app.Activity;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Allan on 06/03/14.
 */
public class AboutActivity extends Activity{

    private TextView aboutTextView;
    private ImageButton allanDuarteImageButton;
    private ImageButton marceloDuarteImageButton;

    @Override
    protected void onCreate(Bundle savedStateInstance)
    {
        super.onCreate(savedStateInstance);
        setContentView(R.layout.about_activity);

        aboutTextView = (TextView) findViewById(R.id.aboutTextView);

        allanDuarteImageButton = (ImageButton) findViewById(R.id.allanDuarteImageButton);
        allanDuarteImageButton.setOnClickListener(allanDuarteImageButtonClick);
        marceloDuarteImageButton = (ImageButton) findViewById(R.id.marceloDuarteImageButton);
        marceloDuarteImageButton.setOnClickListener(marceloDuarteImageButtonClick);
    }

    public View.OnClickListener allanDuarteImageButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openSendEmail("allan.sduarte@gmail.com");
        }
    };

    public View.OnClickListener marceloDuarteImageButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openSendEmail("marceloduarte14@gmail.com");
        }
    };

    private void openSendEmail(String uri)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", uri, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contato - Q Time é Teu?");
        startActivity(Intent.createChooser(emailIntent, null));
    }

}