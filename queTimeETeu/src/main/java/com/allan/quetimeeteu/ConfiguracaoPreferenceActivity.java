package com.allan.quetimeeteu;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

/**
 * Created by Allan on 18/02/14.
 */
public class ConfiguracaoPreferenceActivity extends PreferenceActivity {

    ListPreference prefNivel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.configuracao);

        prefNivel = (ListPreference) findPreference(getString(R.string.nivel_key));
        //prefNivel.getValue();
        //prefNivel.setOnPreferenceChangeListener(prefNivelChange);

    }

    public static String Read(Context context, final String key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(key, "");
    }

    public static void Write(Context context, final String key, final String value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        ConfiguracaoPreferenceActivity.Read(this, "prefNivelDificuldade");
    }
}
