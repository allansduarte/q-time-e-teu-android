package com.allan.quetimeeteu;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by Allan on 06/03/14.
 */
public class GamePlay extends FragmentActivity{

    private MainActivity mainActivity;

    public GamePlay(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    public void printColorButton(Integer color, Button guessButton)
    {
        if( mainActivity.mainController.getDifficult() == InfoResultActivity.EASY)
            guessButton.setTextColor(color);
    }

    public void hideOrShowNameTeam()
    {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mainActivity.flagImageView.getLayoutParams());

        if(mainActivity.mainController.getDifficult() == InfoResultActivity.HARD){
            mainActivity.teamNameTextView.setVisibility(View.GONE);
            lp.setMargins(0, 13, 0, 13);
            mainActivity.flagImageView.setLayoutParams(lp);
        } else{
            mainActivity.teamNameTextView.setVisibility(View.VISIBLE);
            lp.setMargins(0, 0, 0, 0);
            mainActivity.flagImageView.setLayoutParams(lp);
        }
    }

}
