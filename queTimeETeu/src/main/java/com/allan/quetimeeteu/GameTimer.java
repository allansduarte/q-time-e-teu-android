package com.allan.quetimeeteu;

import android.os.CountDownTimer;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TableRow;

/**
 * Created by Allan on 09/02/14.
 */
public class GameTimer extends CountDownTimer{

    public MainActivity mainActivity;
    private Animation shakeAnimation;

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public GameTimer(long millisInFuture, long countDownInterval, MainActivity mainActivity) {
        super(millisInFuture, countDownInterval);
        this.mainActivity = mainActivity;
        shakeAnimation = AnimationUtils.loadAnimation(mainActivity, R.anim.final_time);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        try{

            MenuItem item = mainActivity.menu.findItem(R.id.time);
            int timeInt = (int) millisUntilFinished / 1000;
            String timeText = "Tempo: " + timeInt;

            mainActivity.setTempoAtual(timeInt);
            item.setTitle(timeText);

            if(timeInt <= 10)
            {
                for(int row = 0; row < 3; row++){
                    if(((LinearLayout) ((TableRow) mainActivity.buttonTableLayout.getChildAt(row)).getChildAt(0)).getChildCount() > 0)
                        for(int column = 0; column < 3; column++){
                            ((LinearLayout) ((TableRow) mainActivity.buttonTableLayout.getChildAt(row)).getChildAt(0)).getChildAt(column).startAnimation(shakeAnimation);
                        }
                }
            }
        } catch (NullPointerException e){

        }
    }

    @Override
    public void onFinish() {
        mainActivity.mainController.getSoundPool().play(
            mainActivity.mainController.getSoundMap().get(MainController.APITO_FINAL),
            1,
            1,
            1,
            0,
            1f
        );

        mainActivity.mainController.setGameComplete(false);
        this.mainActivity.mainController.showResults();
    }
}
