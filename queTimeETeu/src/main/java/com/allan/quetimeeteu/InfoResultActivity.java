package com.allan.quetimeeteu;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Allan on 02/02/14.
 */
public class InfoResultActivity extends Activity {

    private ProgressBar completedProgressBar;
    private TextView difficultTextView;
    private ImageView iconeImageView;
    private TextView percentTextView;
    private Button newGameButton;

    public final static int EASY    = 1;
    public final static int MEDIUM  = 2;
    public final static int HARD    = 3;

    private boolean gameComplete;

    private TableLayout quizListTableLayout;
    private ArrayList<String> quizImageList;

    private static final String IMAGE_EXTENSION = ".png";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.info_result);
        completedProgressBar = (ProgressBar) findViewById(R.id.completedProgressBar);
        iconeImageView       = (ImageView) findViewById(R.id.iconeImageView);
        difficultTextView    = (TextView) findViewById(R.id.difficultTextView);
        percentTextView      = (TextView) findViewById(R.id.percentTextView);
        newGameButton        = (Button) findViewById(R.id.newGameButton);
        newGameButton.setOnClickListener(newGameButtonClick);
        quizListTableLayout  = (TableLayout) findViewById(R.id.quizListTableLayout);

        Bundle extrasMainController = getIntent().getExtras();

        if(extrasMainController != null)
        {
            quizImageList = new ArrayList<String>();
            quizImageList = extrasMainController.getStringArrayList("quizList");
            setGameComplete(extrasMainController.getBoolean("statusGame"));
            Double percent = extrasMainController.getDouble("completedProgressBar");
            completedProgressBar.setProgress( percent.intValue());

            switch (extrasMainController.getInt("difficultTextView"))
            {
                case EASY:
                    difficultTextView.setText(getResources().getString(R.string.nivelFacil));
                    iconeImageView.setImageResource(R.drawable.icone_facil);
                    break;
                case MEDIUM:
                    difficultTextView.setText(getResources().getString(R.string.nivelMedio));
                    iconeImageView.setImageResource(R.drawable.icone_medio);
                    break;
                case HARD:
                    difficultTextView.setText(getResources().getString(R.string.nivelDificil));
                    iconeImageView.setImageResource(R.drawable.icone_dificil);
                    break;
            }

            percentTextView.setText(String.format("%.2f", percent) + " %");

            if(Build.VERSION.SDK_INT >= 11){
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                AssetManager assets = getAssets();
                for(int row = 0; row < 2; row++)
                {
                    TableRow currentTableRow = (TableRow) quizListTableLayout.getChildAt(row);
                    for (int column = 0; column < 5; column++)
                    {
                        ImageView newTeamImage = (ImageView) inflater.inflate(R.layout.info_result_image_team, null);

                        int key = column;
                        if(row == 1){
                            key += 5;
                        }

                        String path = quizImageList.get(key);

                        InputStream stream;
                        try{
                            String paths[] = path.split("-");
                            stream = assets.open(paths[0] + "/" + paths[0]+"-"+paths[1] + "/"+ paths[0]+"-"+paths[1]+"-"+paths[2]+"-"+paths[3]+MainActivity.IMAGE_EXTENSION);
                            Drawable imageTeam = Drawable.createFromStream(stream, path);
                            newTeamImage.setImageDrawable(imageTeam);
                        } catch (IOException e){
                            Log.e(InfoResultActivity.class.getName(), "Erro ao carregar nome de arquivo da imagem!");
                        }

                        currentTableRow.addView(newTeamImage);
                    }
                }
            } else{
                HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
                horizontalScrollView.setVisibility(View.GONE);

                TextView infoTeamUsedTextView = (TextView) findViewById(R.id.infoTeamUsedTextView);
                infoTeamUsedTextView.setVisibility(View.GONE);
            }
        }
    }

    public View.OnClickListener newGameButtonClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", true);
            finish();
        }
    };

    public boolean isGameComplete() {
        return gameComplete;
    }

    public void setGameComplete(boolean gameComplete) {
        this.gameComplete = gameComplete;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onCreateOptionsMenu(Menu mMenu)
    {
        super.onCreateOptionsMenu(mMenu);
        ActionBar actionBar = getActionBar();

        if(isGameComplete())
            actionBar.setTitle(getResources().getString(R.string.complete_quiz));
        else
            actionBar.setTitle(getResources().getString(R.string.game_over));

        return true;
    }

}
