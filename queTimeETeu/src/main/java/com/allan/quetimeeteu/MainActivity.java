package com.allan.quetimeeteu;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class MainActivity extends ActionBarActivity {

    //String usada ao se registrar mensagens erros
    private static final String TAG = "QueTimeETeu Activity";

    private ArrayList<String> fileNameList;//nomes de arquivo das bandeiras

    private ArrayList<String> textoBotoes;

    private SharedPreferences listaRespostas;
    private SharedPreferences savedResources;

    private String correctAnswer;//país correto para a bandeira correta
    private int guessRows;//números de linhas que exibem escolha
    private Random random;//gerador de números aleatórios
    private Handler handler;//usado para atrasar o carregamento da próxima bandeira


    private Map<String, Boolean> regionsMap;//quais regiões estão habilitadas
    private Map<String, Boolean> campeonatosMap;

    private Animation shakeAnimation;//animação para palpite incorreto


    public ImageView flagImageView;//exibe uma bandeira
    public TableLayout buttonTableLayout;//tabela de componentes Button de resposta
    private TextView answerTextView;
    public TextView teamNameTextView;
    private ScrollView buttonsScrollView;

    //constantes para cada id de menu
    private final int CHOICES_MENU_ID = Menu.FIRST;
    private final int REGIONS_MENU_ID = Menu.FIRST + 1;

    public Menu menu;

    private boolean segundoPlano = false;

    // Constantes para salvamento do jogo
    private static final String QUESTION_TEXT = "QUESTION_TEXT";
    private static final String FILENAME_LIST = "FILENAME_LIST";
    private static final String QUIZ_LIST     = "QUIZ_LIST";
    private static final String QUIZ_LOG_LIST = "QUIZ_LOG_LIST";
    private static final String TEXTO_BOTOES  = "TEXTO_BOTOES";
    //private static final String REGIONS_LIST  = "REGIONS_LIST";
    //private static final String CAMPEONATO_LIST  = "CAMPEONATOS_LIST";
    private static final String GUESS_ROW_SVI  = "CAMPEONATOS_LIST";
    private static final String CORRECT_ANSWER_STRING_SVI  = "CORRECT_ANSWER_STRING";
    private static final String CORRECT_ANSWER_INT_SVI  = "CORRECT_ANSWER_INT";
    private static final String TEMPO_ATUAL = "TEMPO_ATUAL";

    public MainController mainController;
    private int tempoAtual;

    private GameTimer timer;

    public final static int TEMPOTOTALDECORRIDO     = 30000;
    public final static int INTERVALOTEMPODECORRIDO = 1000;

    public final static String IMAGE_EXTENSION = ".png";

    private android.support.v7.app.ActionBar actionBar;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        //obtém referência para componentes da interface gráfica do usuário
        flagImageView          = (ImageView) findViewById(R.id.flagImageView);
        buttonTableLayout      = (TableLayout) findViewById(R.id.buttonTableLayout);
        answerTextView         = (TextView) findViewById(R.id.answerTextView);
        teamNameTextView       = (TextView) findViewById(R.id.teamNameTextView);
        buttonsScrollView      = (ScrollView) findViewById(R.id.buttonsScrollView);

        random              = new Random();//inicializa o gerador de números aleatórios
        handler             = new Handler();//usado para efetuar as operação postergadas

        mainController = new MainController(MainActivity.this);

        regionsMap          = new HashMap<String, Boolean>();//declarado varável chave/valor das regiões dos países
        campeonatosMap      = new HashMap<String, Boolean>();

        //carrega a animação de tremular a  bandeira se a resposta estiver errada
        shakeAnimation      = AnimationUtils.loadAnimation(this, R.anim.incorrect_shake);
        shakeAnimation.setRepeatCount(3);//a animação se repete 3 vezes

        listaRespostas = getSharedPreferences("respostas", MODE_PRIVATE);
        savedResources = getSharedPreferences("savedResources", MODE_PRIVATE);
        mapearListaArquivos();
        actionBar = getSupportActionBar();
        int numberOfRows = 1;
        try{
            numberOfRows = Integer.parseInt(ConfiguracaoPreferenceActivity.Read(MainActivity.this, "prefNivelDificuldade"));
        } catch (NumberFormatException e){
            numberOfRows = 1;
        }

        mainController.setDifficult(numberOfRows);

        GamePlay gamePlay = new GamePlay(MainActivity.this);

        gamePlay.hideOrShowNameTeam();

        if (savedInstanceState == null)
        {
            fileNameList        = new ArrayList<String>();//declarado objeto, que será populado uma lista de nomes de arquivo de imagens
            mainController.setQuizTeamList(new ArrayList<String>());//declarado objeto, que será populado as bandeiras teste
            setTextoBotoes(new ArrayList<String>());

            correctAnswer = "";
            guessRows           = numberOfRows;// Declarado padrão de nível de escolha das respostas
            mainController.setDifficult(numberOfRows);

            setSegundoPlano(false);

            resetQuiz();//inicia um novo teste

        } else {
            /*mainController.setQuizTeamListLog(savedInstanceState.getStringArrayList(QUIZ_LOG_LIST));
            correctAnswer       = savedInstanceState.getString(CORRECT_ANSWER_STRING_SVI);
            mainController.setCorrectAnswers(savedInstanceState.getInt(CORRECT_ANSWER_INT_SVI));
            fileNameList        = savedInstanceState.getStringArrayList(FILENAME_LIST);
            mainController.setQuizTeamList(savedInstanceState.getStringArrayList(QUIZ_LIST));
            setTextoBotoes(savedInstanceState.getStringArrayList(TEXTO_BOTOES));
            guessRows           = savedInstanceState.getInt(GUESS_ROW_SVI);
            setTempoAtual(savedInstanceState.getInt(TEMPO_ATUAL));

            long tempoAtualMillis = savedInstanceState.getInt(TEMPO_ATUAL) * 1000;
            timer = new GameTimer(tempoAtualMillis, 1000, MainActivity.this);*/

            setSegundoPlano(true);
            loadNextTeam();
        }
    }

    private Animation fadeIn(){
        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        return fadeIn;
    }

    private void mapearListaArquivos()
    {
        //obtém o array de regiões dos países do futebol
        String[] regionNames = getResources().getStringArray(R.array.regionsList);

        //por padrão são escolhido países de todas as regiões
        for (String region : regionNames)
        {
            regionsMap.put(region, true);
        }

        String[] campeonatos = getResources().getStringArray(R.array.campeonatosList);
        for (String campeonato : campeonatos)
        {
            campeonatosMap.put(campeonato, true);
        }
    }

    //prepara e inicia novo teste
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public void resetQuiz()
    {
        //usa o componente AssetManager para obter os nomes de arquivo de imagem de bandeira apenas das regiões habilitadas
        AssetManager assets = getAssets();//obtém o componente AssetManager do app

        GamePlay gamePlay = new GamePlay(MainActivity.this);
        gamePlay.hideOrShowNameTeam();

        getTextoBotoes().clear();
        fileNameList.clear();//limpa/esvazia lista de nome de arquivos das bandeiras para serem apresentados
        mainController.getQuizTeamList().clear();//limpa/esvazia lista de respostas corretas
        mainController.getQuizTeamListLog().clear();

        try {

            Set<String> regions = regionsMap.keySet();//obtém o conjunto de regiões

            //faz loop por cada região
            SharedPreferences.Editor listaRespostasEditor = listaRespostas.edit();
            listaRespostasEditor.clear();
            for (String region : regions)
            {
                if (regionsMap.get(region))//se a região estiver habilitada
                {
                    Set<String> campeonatos = campeonatosMap.keySet();
                    for(String campeonato : campeonatos)
                    {
                        String[] pathFileName = campeonato.split("-");

                        if(campeonatosMap.get(campeonato) && region.equals(pathFileName[0])){
                            //obtém uma  lista de todos os arquivos de imagem de bandeira dessa região
                            String[] paths = assets.list(region+"/"+campeonato);
                            for (String path : paths)
                            {

                                String pathName = path.replace(IMAGE_EXTENSION, "");
                                String[] pathNameOptions = pathName.split("-");

                                fileNameList.add(pathName);
                                String value = procuraItem(pathNameOptions[2]);
                                listaRespostasEditor.putString(pathNameOptions[2]+"_"+pathNameOptions[3], value);
                                listaRespostasEditor.apply();
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Erro ao carregar nome de arquivo da imagem!");
        }

        mainController.setCorrectAnswers(0);//zera o número de respostas informadas
        mainController.setTotalGuesses(0);//zera o número total de palpite informado pelo usuário

        //adiciona 10 nomes de arquivo aleatórios em quizCountriesList
        int flagCounter   = 1;
        int numberOfFlags = fileNameList.size();

        while (flagCounter <= 10 && numberOfFlags > 0)
        {
            int randomIndex = random.nextInt(numberOfFlags);//índice aleatório

            //obtém o nome do arquivo aleatório
            String fileName = fileNameList.get(randomIndex);

            //se a região está habilitada e ainda não foi escolhida
            if (!mainController.getQuizTeamList().contains(fileName))
            {
                mainController.getQuizTeamList().add(fileName);//adiciona arquivo na lista
                mainController.getQuizTeamListLog().add(fileName);// clona a lista
                ++flagCounter;
            }
        }

        if(numberOfFlags > 0){
            loadNextTeam();//inicia o teste, carregando a primeira bandeira
        } else{
            // Fazer algum tratamento
        }
    }

    //após o usuário advinhar uma bandeira correta, carrega a próxima
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void loadNextTeam()
    {
        int textNumber = mainController.getCorrectAnswers() + 1;

        String nextImageName = "";
        //obtém o nome do arquivo da próxima bandeira correta e o remove da lista


        buttonsScrollView.scrollTo(0 ,0);
        buttonsScrollView.invalidate();

        nextImageName = mainController.getQuizTeamList().remove(0);
        correctAnswer        = nextImageName;//atualiza a resposta correta
        //exibe o número da pergunta atual
        String currentQuestionText = getResources().getString(R.string.question) + " " +
                textNumber + " " +
                getResources().getString(R.string.of) + " 10";

        actionBar.setTitle(currentQuestionText);

        answerTextView.setText("");//limpa answerTextView
        mainController.getCorrectAnswers();
        //usa AssetManager para carregar a próxima imagem da pasta assets
        AssetManager assets = getAssets();//obtém o componente AssetManager do app
        InputStream stream;//usado para ler as imagens da bandeira

        try {

            String[] paths = correctAnswer.split("-");

            //obtém um componente InputStream para o asset, que representa a próxima bandeira
            stream = assets.open(paths[0] + "/" + paths[0]+"-"+paths[1] + "/"+ paths[0]+"-"+paths[1]+"-"+paths[2]+"-"+paths[3]+IMAGE_EXTENSION);
            //String svgPath = paths[0] + "/" + paths[0]+"-"+paths[1] + "/"+ paths[0]+"-"+paths[1]+"-"+paths[2]+"-"+paths[3]+IMAGE_EXTENSION;

            //SVGBuilder svg = new SVGBuilder();
            //Picture team = svg.readFromAsset(assets, svgPath).build().getPicture();

            //carrega o asset como um elemento Drawable e exibe no componente flagImageView
            Drawable team = Drawable.createFromStream(stream, correctAnswer);

            flagImageView.setImageDrawable(team);

            teamNameTextView.setText(getTeamName(correctAnswer));

        } catch (IOException e)
        {
            Log.e(TAG, "Erro ao carregar a imagem " + nextImageName, e);
        }

            //limpa os componentes Button de respota anteriores de TableRows
            for (int row = 0; row < buttonTableLayout.getChildCount(); row++)
            {
                TableRow currentTableRow = (TableRow) buttonTableLayout.getChildAt(row);
                ( (LinearLayout) currentTableRow.getChildAt(0)).removeAllViews();
            }

            Collections.shuffle(fileNameList);//embaralha os nomes de arquivo

            //coloca o arquivo correto no final de fileNameList
            int correct = fileNameList.indexOf(correctAnswer);
            fileNameList.add(fileNameList.remove(correct));
            getTextoBotoes().clear();

        //obtém uma referência para o serviço LayoutInflater
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //adiciona 3, 6 ou 9 componentes Button de resposta, de acordo com o valor de guessRows
        for (int row = 0; row < guessRows; row++)
        {

            TableRow currentTableRow = getTableRow(row);
            LinearLayout currentLinearLayout = (LinearLayout) currentTableRow.getChildAt(0);
            int escape = 0;// Em caso de já conter o time, vai ocorrer travamento no loop, a variável escapa o travamento
            //anexa os componentes button no currentTableRow
            for (int column = 0; column < 3; column++)
            {

                //infla guess_button.xml para criar novo componente Button
                Button newGuessButton = (Button) inflater.inflate(R.layout.guess_button, null);

                //obtém o nome do país e o configura como texto de newGuessButton
                int randomTeam = random.nextInt(fileNameList.size());
                String fileName = fileNameList.get( randomTeam);
                String nomeTime = getLocalTeamName(fileName);
                if(!getTextoBotoes().contains(nomeTime) && !nomeTime.equalsIgnoreCase(getLocalTeamName(correctAnswer)))
                {
                    getTextoBotoes().add(nomeTime);
                    newGuessButton.setText(nomeTime);
                    newGuessButton.setSoundEffectsEnabled(false);
                    //registra answerButtonListener para responder aos cliques nos botões
                    newGuessButton.setOnClickListener(guessButtonListener);
                    currentLinearLayout.addView(newGuessButton);
                } else {
                    --column;
                    // Não registra answerButtonListener para responder aos cliques nos botões,
                    // pois nesse caso foi identificado que o texto de botão já existe
                }
            }
        }

            int key = 0;
            //substiuí aleatoriamente um componente Button pela resposta correta
            int row = random.nextInt(guessRows); // escolhe a linha aleatoriamente

            switch (row){
                case 1:
                    key = 3;
                    break;
                case 2:
                    key = 6;
                    break;
            }

            int column = random.nextInt(3); // escolhe a coluna aleatoriamente
            key += column;
            TableRow randomTableRow = getTableRow(row); // obtém o componente TableRow
            LinearLayout randomLinearLayout = (LinearLayout) randomTableRow.getChildAt(0);
            String localTeamName = getLocalTeamName(correctAnswer);
            getTextoBotoes().remove(key);
            getTextoBotoes().add(key, localTeamName);
            ((Button)randomLinearLayout.getChildAt(column)).setText(localTeamName);

            try{
                MenuItem item = menu.findItem(R.id.time);
                item.setTitle("Tempo: 30");
                timer.cancel();
                timer = new GameTimer(TEMPOTOTALDECORRIDO, INTERVALOTEMPODECORRIDO, MainActivity.this);
            } catch (NullPointerException e){
                //tratamento
            }

        if(timer != null){

            timer.start();
        }
        setSegundoPlano(false);// Após carregar a bandeira não está mais em segundo plano
    }

    //chamado quando o usuário seleciona uma resposta
    private void submitGuess(Button guessButton)
    {

        String guess = guessButton.getText().toString();
        String answer = getLocalTeamName(correctAnswer);
        mainController.setTotalGuesses(mainController.getTotalGuesses() +1); //incrementa o número de palpites informados
        GamePlay gamePlay = new GamePlay(MainActivity.this);

        //se o palpite estiver correto
        if (guess.equals(answer))
        {
            gamePlay.printColorButton(getResources().getColor(R.color.correct_answer), guessButton);

            guessButton.startAnimation(fadeIn());

            disableButtons();//desabilita todos os botões de resposta
            mainController.setCorrectAnswers(mainController.getCorrectAnswers() + 1);//incrementa o número de respostas corretas

            mainController.setToastMessage(mainController.buildMessage(answer + "!"));
            mainController.getToastMessage().show();

            //se o usuário identificou 10 bandeiras corretas
            if (mainController.getCorrectAnswers() == 10)
            {

                timer.cancel();
                mainController.setGameComplete(true);
                mainController.showResults();
            }
            else // a resposta está correta, mas o teste não acabou
            {

                //carrega a próxima bandeira, após um atraso de 1 segundo
                handler.postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                loadNextTeam();
                            }
                        } ,1000// 1000 milissegundos (1 segundo) de atraso
                );
            }
        }
        else//o palpite está errado
        {
            if( mainController.getDifficult() == InfoResultActivity.EASY)
                gamePlay.printColorButton(getResources().getColor(R.color.incorrect_answer), guessButton);

            guessButton.startAnimation(fadeIn());

            //exibe a animação
            flagImageView.startAnimation(shakeAnimation);
            guessButton.startAnimation(shakeAnimation);

            mainController.setToastMessage(mainController.buildMessage(getResources().getString(R.string.incorrect_answer)));
            mainController.getToastMessage().show();
        }
    }

    //retorna o componente TableRow especificado
    private TableRow getTableRow(int row)
    {
        return (TableRow) buttonTableLayout.getChildAt(row);
    }

    //analisa o nome de arquivo de bandeira de país e retorna o nome do país
    private String getTeamName(String name)
    {

        String retorno = "";

        String[] nameOptions = name.split("-");
        String teamName = procuraItem(nameOptions[2] + "_" + nameOptions[3]);

        String[] teamNameOptions = teamName.split("-");
        teamName = teamNameOptions[1].replace('_', ' ');
        retorno = teamName;

        return retorno;
    }

    private String getLocalTeamName(String name)
    {

        String retorno = "";
        String[] nameOptions = name.split("-");
        String respostasList = procuraItem(nameOptions[2] + "_" + nameOptions[3]);

        String[] teamNameOptions = respostasList.split("-");
        String teamName = teamNameOptions[2].replace('_', ' ');
        String stateName = teamNameOptions[3];
        retorno = teamName+"-"+stateName;

        return retorno;
    }

    //método utilitário, que desabilita todos os botões de resposta
    public void disableButtons()
    {
        for(int row = 0; row < 3; row++){
            if(((LinearLayout) ((TableRow) buttonTableLayout.getChildAt(row)).getChildAt(0)).getChildCount() > 0)
                for(int column = 0; column < 3; column++){
                    ((LinearLayout) ((TableRow) buttonTableLayout.getChildAt(row)).getChildAt(0)).getChildAt(column).setEnabled(false);
                }
        }
    }


    public String procuraItem(String itemProcurado)
    {
        String retorno = "";
        String[] itens = getResources().getStringArray(R.array.respostasList);

        for(String item : itens)
        {
            String[] itemArray = item.split("-");
            if(itemProcurado.equals(itemArray[0]))
            {
                retorno = item;
            }
        }

        return retorno;
    }

    //chamado quando um botão de palpite é tocado
    private View.OnClickListener guessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mainController.getSoundPool().play(
                    mainController.getSoundMap().get(mainController.KICK),
                    1,
                    1,
                    1,
                    0,
                    1f
            );

            submitGuess((Button) v);//passa o componente Button selecionado para submitGuess
        }
    };

    public ArrayList<String> getTextoBotoes() {
        return textoBotoes;
    }

    public void setTextoBotoes(ArrayList<String> textoBotoes) {
        this.textoBotoes = textoBotoes;
    }

    public int getTempoAtual() {
        return this.tempoAtual;
    }

    public void setTempoAtual(int tempoAtual) {
        this.tempoAtual = tempoAtual;
    }

    public boolean isSegundoPlano() {
        return segundoPlano;
    }

    public void setSegundoPlano(boolean segundoPlano) {
        this.segundoPlano = segundoPlano;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu mMenu)
    {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, mMenu);
        this.menu = mMenu;
        MenuItem item = menu.findItem(R.id.time);

        super.onCreateOptionsMenu(mMenu);

        if(getTempoAtual() == 0){

            item.setTitle("Tempo: 30");
            timer = new GameTimer(TEMPOTOTALDECORRIDO, INTERVALOTEMPODECORRIDO, MainActivity.this);
            timer.start();
        } else {

            item.setTitle("Tempo: " + getTempoAtual());
        }

        //adiciona as opções "Quantidade de opções" e "Regiões"
        menu.add(Menu.NONE, CHOICES_MENU_ID, Menu.NONE, R.string.choices);
        //menu.add(Menu.NONE, REGIONS_MENU_ID, Menu.NONE, R.string.regions);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/

        switch (item.getItemId()) {
            case CHOICES_MENU_ID:

                //cria uma lista dos número de escolhas de resposta possíveis
                final String[] possibleChoices = getResources().getStringArray(R.array.guessesList);

                //cria um novo AlertDialog.Builder e configura seu título
                AlertDialog.Builder choicesBuilder = new AlertDialog.Builder(this);
                choicesBuilder.setTitle(R.string.choices);
                //adiciona possibleChoices itens em Dialog e configura o comportamento quando um dos itens é clicado

                choicesBuilder.setItems(R.array.guessesList,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int item) {

                                item = item + 1;
                                ConfiguracaoPreferenceActivity.Write(MainActivity.this, "prefNivelDificuldade", String.valueOf(item));
                                //atualiza guessRows de acordo com a escolha do usuário
                                guessRows = item;
                                mainController.setDifficult(item);
                                resetQuiz();//reconfigura o teste
                            }
                        }
                );

                //cria um AlertDialog a partir de um Builder
                AlertDialog choicesDialog = choicesBuilder.create();//constrói o Dialog
               /* ListView listViewDialog = choicesDialog.getListView();

                switch (mainController.getDifficult()){
                    case InfoResultActivity.EASY:
                        listViewDialog.getChildAt(0).setEnabled(false);
                        break;
                    case InfoResultActivity.MEDIUM:
                        listViewDialog.getChildAt(1).setEnabled(false);
                        break;
                    case InfoResultActivity.HARD:
                        listViewDialog.getChildAt(2).setEnabled(false);
                        break;
                }*/

                choicesDialog.show();//mostra o Dialog

            return true;
            case REGIONS_MENU_ID:

                //obtém o array de reigiões do mundo
                final String[] regionNames = regionsMap.keySet().toArray(new String[regionsMap.size()]);

                //array booleano que representa se cada região está habilitada
                boolean[] regionsEnabled = new boolean[regionsMap.size()];
                for (int i = 0; i < regionsEnabled.length; i++)
                {
                    regionsEnabled[i] = regionsMap.get(regionNames[i]);
                }

                //cria um AlertDialog.Builder e configura o seu título
                AlertDialog.Builder regionsBuilder = new AlertDialog.Builder(this);
                regionsBuilder.setTitle(R.string.regions);

                //substitui _ por espaço nos nomes de região para propósito de exibição
                String[] displayNames = new String [regionNames.length];
                for (int i = 0; i < regionNames.length; i++)
                {
                    displayNames[i] = regionNames[i].replace('-', ' ');

                    //adiciona displayNames no componente Dialog e configura o comportamento de quando um dos itens é clicado
                    regionsBuilder.setMultiChoiceItems(
                            displayNames, regionsEnabled,
                            new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                                    //inclui ou exclui a região clicada, dependendo de estar marcada ou não
                                    regionsMap.put(regionNames[which].toString(), isChecked);
                                }
                            }
                    );
                }

                //reconfigura o teste quando o usuário pressiona o botão "Reset Quiz"
                regionsBuilder.setPositiveButton(R.string.reset_quiz,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                resetQuiz();//reconfigura o teste
                            }
                        }
                );

                //cria uma caixa de diálogo a partir de um Builder
                AlertDialog regionsDialog = regionsBuilder.create();
                regionsDialog.show();//mostra o Dialog

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        /*bundle.putStringArrayList(FILENAME_LIST, fileNameList);
        bundle.putStringArrayList(QUIZ_LOG_LIST, mainController.getQuizTeamListLog());
        bundle.putStringArrayList(QUIZ_LIST, mainController.getQuizTeamList());
        bundle.putStringArrayList(TEXTO_BOTOES, getTextoBotoes());
        bundle.putInt(GUESS_ROW_SVI, guessRows);
        bundle.putString(CORRECT_ANSWER_STRING_SVI, correctAnswer);
        bundle.putInt(CORRECT_ANSWER_INT_SVI, mainController.getCorrectAnswers());
        bundle.putInt(TEMPO_ATUAL, getTempoAtual());*/

        SharedPreferences.Editor savedResourcesEdit = savedResources.edit();
        savedResourcesEdit.clear();
        savedResourcesEdit.putInt(TEMPO_ATUAL, getTempoAtual());
        if(savedResourcesEdit.commit()){
            savedResourcesEdit.apply();
        } else{
            savedResourcesEdit.clear();
            savedResourcesEdit.apply();
        }

        setSegundoPlano(true);
        timer.cancel();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == IntentManagerActivity.INFORESULT_ACTIVITY)
        {
            timer = new GameTimer(TEMPOTOTALDECORRIDO, INTERVALOTEMPODECORRIDO, MainActivity.this);
            setSegundoPlano(false);
            resetQuiz();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        timer.cancel();

        if(mainController.getToastMessage() != null)
            mainController.getToastMessage().cancel();
    }

    /*@Override
    protected void onResume()
    {
        super.onResume();
    }*/

    /*@Override
    protected void onStop()
    {
        super.onStop();
        setSegundoPlano(true);// Informa ao app que deve receber os recursos salvos quando estiver em segundo plano
        handler.removeCallbacks(timer);// Desativa a thread do tempo do quiz
    }*/

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if(isSegundoPlano())
        {
            setTempoAtual(savedResources.getInt(TEMPO_ATUAL, 0));
            if(getTempoAtual() > 0){

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Deseja continuar o jogo ?")
                        .setCancelable(false)
                        .setPositiveButton("Sim", clickEventSim)
                        .setNegativeButton("Não", clickEventNao)
                        .create()
                        .show();
            } else {
                Intent intent = new Intent(MainActivity.this, MenuNavigationActivity.class);
                startActivity(intent);
            }
        }
    }

    DialogInterface.OnClickListener clickEventSim = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            timer = new GameTimer(getTempoAtual() * 1000, INTERVALOTEMPODECORRIDO, MainActivity.this);
            timer.start();
        }
    };

    DialogInterface.OnClickListener clickEventNao = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent(MainActivity.this, MenuNavigationActivity.class);
            startActivity(intent);
        }
    };
}
