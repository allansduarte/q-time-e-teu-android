package com.allan.quetimeeteu;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Allan on 30/01/14.
 */
public class MainController extends FragmentActivity{

    private int totalGuesses;//número de palpites dados
    private int correctAnswers;//número de palpites corretos
    private int difficult = 0;
    private Toast toastMessage;

    public boolean gameComplete = false;
    public Context cMainActivity;
    public MainActivity mainActivity;
    public boolean newGame;
    public SoundPool soundPool;
    public Map<Integer, Integer> soundMap;
    public static final int APITO_FINAL = 0;
    public static final int KICK = 1;

    private ArrayList<String> quizTeamList;//nomes dos países do teste
    private ArrayList<String> quizTeamListLog = new ArrayList<String>();

    public MainController(MainActivity mainActivity)
    {
        this.cMainActivity = mainActivity;
        this.mainActivity  = mainActivity;
        setNewGame(false);

        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

        soundMap = new HashMap<Integer, Integer>();
        soundMap.put(APITO_FINAL, soundPool.load(cMainActivity, R.raw.apito_de_futebol,1));
        soundMap.put(KICK, soundPool.load(cMainActivity, R.raw.kick,1));

        //this.mainActivity  = (MainActivity) mainActivity;
    }

    public Toast buildMessage(String message)
    {
        Toast toast = Toast.makeText(cMainActivity, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        return toast;
    }

    public void showResults()
    {

            double percent  = 0;

            if(getTotalGuesses() > 0)
            {
                if(getCorrectAnswers() == 10){
                    percent = (1000 / (double) getTotalGuesses());
                } else{

                    percent = ( ( ( getCorrectAnswers() * 100) / (double) getTotalGuesses() )  / 100);
                    if(percent == 1){
                        percent = (percent * getCorrectAnswers()) * 10;
                    } else{
                        percent = (percent * 10) * getCorrectAnswers();
                    }
                }


            }
            else {
                percent = 0;
            }

        if(getToastMessage() != null)
            getToastMessage().cancel();

        Intent infoGame = new Intent(cMainActivity, InfoResultActivity.class);
        infoGame.putExtra("completedProgressBar", percent);
        infoGame.putExtra("difficultTextView", getDifficult());
        infoGame.putExtra("statusGame", isGameComplete());
        infoGame.putExtra("quizList", getQuizTeamListLog());
        mainActivity.startActivityForResult(infoGame, IntentManagerActivity.INFORESULT_ACTIVITY);
    }

    public Toast getToastMessage() {
        return toastMessage;
    }

    public void setToastMessage(Toast toastMessage) {
        this.toastMessage = toastMessage;
    }

    public SoundPool getSoundPool() {
        return soundPool;
    }

    public void setSoundPool(SoundPool soundPool) {
        this.soundPool = soundPool;
    }

    public Map<Integer, Integer> getSoundMap() {
        return soundMap;
    }

    public void setSoundMap(Map<Integer, Integer> soundMap) {
        this.soundMap = soundMap;
    }

    public ArrayList<String> getQuizTeamListLog() {
        return quizTeamListLog;
    }

    public void setQuizTeamListLog(ArrayList<String> quizTeamListLog) {
        this.quizTeamListLog = quizTeamListLog;
    }

    public ArrayList<String> getQuizTeamList() {
        return quizTeamList;
    }

    public void setQuizTeamList(ArrayList<String> quizTeamList) {
        this.quizTeamList = quizTeamList;
    }

    public boolean isGameComplete() {
        return gameComplete;
    }

    public void setGameComplete(boolean gameComplete) {
        this.gameComplete = gameComplete;
    }

    public int getDifficult() {
        return difficult;
    }

    public void setDifficult(int difficult) {
        this.difficult = difficult;
    }

    public boolean isNewGame() {
        return newGame;
    }

    public void setNewGame(boolean newGame) {
        this.newGame = newGame;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(int correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public int getTotalGuesses() {
        return totalGuesses;
    }

    public void setTotalGuesses(int totalGuesses) {
        this.totalGuesses = totalGuesses;
    }

}
