package com.allan.quetimeeteu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

/**
 * Created by Allan on 09/02/14.
 */
public class MenuNavigationActivity extends Activity{

    private Button newQuizButton;
    private Button configuracaoButton;
    private Button aboutButton;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_menu);

        newQuizButton = (Button) findViewById(R.id.newQuiz);
        newQuizButton.setOnClickListener(newQuizButtonClick);

        configuracaoButton = (Button) findViewById(R.id.configuracaoButton);
        configuracaoButton.setOnClickListener(configuracaoButtonClick);

        aboutButton = (Button) findViewById(R.id.aboutButton);
        aboutButton.setOnClickListener(aboutButtonClick);

        animation = AnimationUtils.loadAnimation(MenuNavigationActivity.this, R.anim.fadein);
    }

    public View.OnClickListener newQuizButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(animation);
            Intent intent = new Intent(MenuNavigationActivity.this, MainActivity.class);
            startActivity(intent);
        }
    };

    public View.OnClickListener configuracaoButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(animation);
            Intent intent = new Intent(MenuNavigationActivity.this, ConfiguracaoPreferenceActivity.class);
            startActivity(intent);
        }
    };

    public View.OnClickListener aboutButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.startAnimation(animation);
            Intent intent = new Intent(MenuNavigationActivity.this, AboutActivity.class);
            startActivity(intent);
        }
    };
}
