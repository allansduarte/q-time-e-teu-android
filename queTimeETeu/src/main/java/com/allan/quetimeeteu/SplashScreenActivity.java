package com.allan.quetimeeteu;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by Allan on 09/02/14.
 */
public class SplashScreenActivity extends Activity{

    private boolean mClicou = false;
    private static final long TEMPO_DURACAO     = 3000;
    private static final long TEMPO_INTERVALO   = 1000;
    private CountDownTimer countDownTimer;
    private WebView layoutWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        layoutWebView = (WebView) findViewById(R.id.layoutWebView);
        layoutWebView.setWebChromeClient(new WebChromeClient());

        if(Build.VERSION.SDK_INT >= 11)
            layoutWebView.loadUrl("file:///android_asset/splash_screen.html");
        else
            layoutWebView.loadUrl("file:///android_asset/splash_screen_old_version.html");

        countDownTimer = new CountDownTimer(TEMPO_DURACAO, TEMPO_INTERVALO) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Pode-se customizar
            }

            @Override
            public void onFinish()
            {
                intentMenu();
            }
        };

        countDownTimer.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN ||
                event.getAction() == MotionEvent.ACTION_UP) {

            countDownTimer.cancel();
            intentMenu();
        }

        return true;
    }

    private void intentMenu()
    {
        finish();

        Intent intent = new Intent(SplashScreenActivity.this, MenuNavigationActivity.class);
        startActivity(intent);
    }
}
